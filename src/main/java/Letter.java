import java.util.Scanner;

public class Letter {

  private char self;
  private int occurence = 0;

  public Letter(char self, int occurence ) {
    this.self = self;
  }

  public int getOccurence() {
    return occurence;
  }

  public void setOccurence(int occurence) {
    this.occurence = occurence;
  }

  public char getSelf() {
    return self;
  }

  public void setSelf(char self) {
    this.self = self;
  }
}
