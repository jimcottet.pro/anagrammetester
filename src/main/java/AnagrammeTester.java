import java.lang.reflect.Array;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;
import java.util.*;
public class AnagrammeTester {

  Scanner scanner = new Scanner(System.in);
  Random rand = new Random();

  public String guessAnagramme(String tested, String word) {

    boolean win = false;
    String text = word;
    String textLowCase = text.toLowerCase();
    String answer = "";

    String[] dico = {"ironique","marion", "manoir", "minora", "romain", "marino",
        "romina", "romani", "mirano", "mornai", "normai",  "aimer", "maire", "Marie",
        "ramie", "arime", "meira", "meria", "miera", "raime" };
    List<String> Dico = Arrays.asList(dico);

      String textToCompare = tested;
      String textToCompareLowCase = textToCompare.toLowerCase();
      char[] liste = textToCompareLowCase.toCharArray();
      int x = 0;
      int index = 0;
      ArrayList<Character> letters = new ArrayList<Character>();

      for (char i : liste) {
        if (textLowCase.contains(Character.toString(i))) {
            x++;
        }
      }
      if (x == textLowCase.length() && !textLowCase.equals(textToCompareLowCase) && Dico.contains(textToCompareLowCase)) {
        answer = "Anagramme";
        win = true;
      } else if (textLowCase.equals(textToCompareLowCase)) {
        answer = "Same";
      } else {
        answer = "No Anagramme";
      }
    return answer;
  }

  public String randWords() {
    ArrayList<String> wordList = new ArrayList<String>();
    int randNumber = rand.nextInt(3);
    wordList.add("Marion");
    wordList.add("Aimer");
    wordList.add("Onirique");
    String guessWord = wordList.get(randNumber);
    return guessWord;
  }






}
