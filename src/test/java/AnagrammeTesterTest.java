import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AnagrammeTesterTest {

  @Test
  void testAnagrammeTesterTrue() {
    AnagrammeTester anagrammeTester = new AnagrammeTester();
    assertEquals("Anagramme", anagrammeTester.guessAnagramme("Romain", "Marion"));
  }

  @Test
  void testAnagrammeTesterSame() {
    AnagrammeTester anagrammeTester = new AnagrammeTester();
    assertEquals("Same", anagrammeTester.guessAnagramme("Onirique", "Onirique"));
  }

  @Test
  void testAnagrammeTesterFalse() {
    AnagrammeTester anagrammeTester = new AnagrammeTester();
    assertEquals("No Anagramme", anagrammeTester.guessAnagramme("Ironeque", "Onirique"));
  }


}